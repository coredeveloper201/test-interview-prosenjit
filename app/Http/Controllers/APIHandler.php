<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Embed\Embed;

class APIHandler extends Controller
{
    public function getDateTime(Request $request){
        $this->validate($request, [
            'date_time' => 'date_multi_format:"Y-m-d H:i:s"'
        ]);

        $providedDateTime = strtotime($request->date_time);
        $currentDateTime = date('Y-m-d H:i:s');
        $difference = date_diff($providedDateTime,$currentDateTime);
        $difference = $this->secondsToTime($difference);

        return response()->json([
            'success' => 'Successfully sent!',
            'date_difference' => $difference
        ], 200);
    }

    function secondsToTime($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
    }

    public function getURL(Request $request){
        $this->validate($request, [
            'url' => 'url'
        ]);

        $url = $request->url;
        $info = Embed::create($url);

        $message = 'Site title: '.$info->title.', Description'.$info->description;
       
        return response()->json([
            'success' => 'Successful!',
            'message' => $message
        ], 200);
    }
}
